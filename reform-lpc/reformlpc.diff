Index: arch/arm64/conf/GENERIC
===================================================================
RCS file: /cvs/src/sys/arch/arm64/conf/GENERIC,v
diff -u -p -r1.283 GENERIC
--- arch/arm64/conf/GENERIC	15 Feb 2024 16:33:54 -0000	1.283
+++ arch/arm64/conf/GENERIC	4 May 2024 03:30:44 -0000
@@ -350,6 +350,9 @@ qcrng*		at fdt?
 qcrtc*		at qcpmic?
 qcsdam*		at qcpmic?
 
+# MNT Reform
+reformlpc*	at spi?
+
 # Sunxi SoCs
 sxipio*		at fdt? early 1	# GPIO pins for leds & PHYs
 gpio*		at sxipio?
Index: dev/fdt/files.fdt
===================================================================
RCS file: /cvs/src/sys/dev/fdt/files.fdt,v
diff -u -p -r1.199 files.fdt
--- dev/fdt/files.fdt	16 Jan 2024 23:37:50 -0000	1.199
+++ dev/fdt/files.fdt	4 May 2024 03:30:46 -0000
@@ -772,3 +772,8 @@ file	dev/fdt/qcsdam.c		qcsdam
 device	tipd
 attach	tipd at i2c
 file	dev/fdt/tipd.c		tipd
+
+# MNT Reform LPC
+device reformlpc
+attach reformlpc at spi
+file   dev/fdt/reformlpc.c              reformlpc
\ No newline at end of file
Index: dev/fdt/reformlpc.c
===================================================================
RCS file: dev/fdt/reformlpc.c
diff -N dev/fdt/reformlpc.c
--- /dev/null	1 Jan 1970 00:00:00 -0000
+++ dev/fdt/reformlpc.c	4 May 2024 03:30:46 -0000
@@ -0,0 +1,316 @@
+/*	$OpenBSD: reformlpc.c,v 1.0 2024/02/28 07:00:00 rayne Exp $	*/
+
+/*
+ * Copyright (c) 2024 Rayne <rayne+openbsd@digitalrayne.net>
+ *
+ * Permission to use, copy, modify, and distribute this software for any
+ * purpose with or without fee is hereby granted, provided that the above
+ * copyright notice and this permission notice appear in all copies.
+ *
+ * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
+ * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
+ * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
+ * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
+ * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
+ * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
+ * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
+ */
+
+#include <sys/param.h>
+#include <sys/kernel.h>
+#include <sys/systm.h>
+#include <sys/device.h>
+#include <sys/malloc.h>
+#include <sys/sensors.h>
+#include <sys/stdint.h>
+
+#include <dev/spi/spivar.h>
+#include <dev/ofw/openfirm.h>
+
+#include <machine/apmvar.h>
+
+#include "apm.h"
+
+extern void (*powerdownfn)(void);
+
+// reform LPC protocol specific
+#define REFORMLPC_MAGIC 0xB5
+#define REFORMLPC_PACKET_SIZE 8
+
+// reform LPC commands
+#define REFORMLPC_CMD_POWER        'p'
+#define REFORMLPC_CMD_STATUS_QUERY 'q'
+
+// reform power arguments
+#define REFORMLPC_POWER_OFF        1
+
+// reform power state
+enum state_t {
+              REFORM_ST_CHARGE,
+              REFORM_ST_OVERVOLTED,
+              REFORM_ST_COOLDOWN,
+              REFORM_ST_UNDERVOLTED,
+              REFORM_ST_MISSING,
+              REFORM_ST_FULLY_CHARGED,
+              REFORM_ST_POWERSAVE
+};
+
+struct reformlpc_query_data {
+	uint8_t		voltage[2];
+        uint8_t         amps[2];
+        uint8_t         bat_percent;
+        uint8_t         status;
+        uint8_t         reserved[2];
+};
+
+const struct {
+	const char *desc;
+	enum sensor_type type;
+} reformlpc_sensors_template[] = {
+#define	REFORMLPC_SENSOR_AC_PRESENCE 0
+	{
+		.desc = "AC present",
+		.type = SENSOR_INDICATOR,
+	},
+#define	REFORMLPC_SENSOR_BATTERY_CHARGING 1
+	{
+		.desc = "Battery charging",
+		.type = SENSOR_INDICATOR,
+	},
+#define	REFORMLPC_SENSOR_BATTERY_VOLTAGE 2
+	{
+		.desc = "Battery voltage",
+		.type = SENSOR_VOLTS_DC,
+	},
+#define	REFORMLPC_SENSOR_BATTERY_CURRENT 3
+	{
+		.desc = "Battery current",
+		.type = SENSOR_AMPS,
+	},
+#define	REFORMLPC_SENSOR_BATTERY_PERCENT 4
+	{
+		.desc = "Battery charge percentage",
+		.type = SENSOR_PERCENT,
+	}
+};
+
+struct reformlpc_softc {
+	struct device		 sc_dev;
+	int			 sc_node;
+
+	/* SPI configuration */
+	spi_tag_t		 sc_spi_tag;
+	struct spi_config	 sc_spi_conf;
+
+        /* sensors */
+	struct ksensor		 sc_sensors[nitems(reformlpc_sensors_template)];
+	struct ksensordev	 sc_sensordev;
+	struct sensor_task	*sc_sensors_update_task;
+};
+
+struct reformlpc_softc *reformlpc_sc;
+
+int	 reformlpc_match(struct device *, void *, void *);
+void	 reformlpc_attach(struct device *, struct device *, void *);
+void	 reformlpc_command(struct reformlpc_softc *, char, char, char *);
+void	 reformlpc_powerdown(void);
+int	 reformlpc_apminfo(struct apm_power_info *);
+void	 reformlpc_sensors_update(void *);
+
+#if NAPM > 0
+struct apm_power_info reformlpc_apmdata;
+#endif
+
+const struct cfattach reformlpc_ca = {
+	sizeof(struct reformlpc_softc),
+        reformlpc_match,
+	reformlpc_attach,
+};
+
+struct cfdriver reformlpc_cd = {
+	NULL, "reformlpc", DV_DULL
+};
+
+int
+reformlpc_match(struct device *parent, void *match, void *aux)
+{
+	struct spi_attach_args *sa = aux;
+
+	// match the reform2 LPC device tree entry
+	if (strcmp(sa->sa_name, "mntre,lpc11u24") == 0)
+		return 1;
+
+	return 0;
+}
+
+void
+reformlpc_attach(struct device *parent, struct device *self, void *aux)
+{
+	struct reformlpc_softc *lpc = (struct reformlpc_softc *)self;
+	struct spi_attach_args *sa = aux;
+
+	// for usage in poweroff fn
+        reformlpc_sc = lpc;
+
+	// configure SPI for communicating with LPC
+	lpc->sc_spi_tag = sa->sa_tag;
+	lpc->sc_node = *(int *)sa->sa_cookie;
+	lpc->sc_spi_conf.sc_bpw = 8;
+	lpc->sc_spi_conf.sc_freq = OF_getpropint(lpc->sc_node, "spi-max-frequency", 0);
+	lpc->sc_spi_conf.sc_cs = OF_getpropint(lpc->sc_node, "reg", 0);
+	lpc->sc_spi_conf.sc_cs_delay = 100;
+ 	lpc->sc_spi_conf.sc_flags |= SPI_CONFIG_CPHA;
+
+	// configure sensors and refresh task
+	lpc->sc_sensors_update_task =
+	    sensor_task_register(lpc, reformlpc_sensors_update, 1);
+	if (lpc->sc_sensors_update_task == NULL) {
+		printf("%s: failure setting sensor update task\n", self->dv_xname);
+		return;
+	}
+
+	// iterate over sensor template and add each sensor definition
+	strlcpy(lpc->sc_sensordev.xname, lpc->sc_dev.dv_xname,
+	    sizeof(lpc->sc_sensordev.xname));
+	for (int i = 0; i < nitems(lpc->sc_sensors); i++) {
+		lpc->sc_sensors[i].type = reformlpc_sensors_template[i].type;
+		strlcpy(lpc->sc_sensors[i].desc, reformlpc_sensors_template[i].desc,
+		    sizeof(lpc->sc_sensors[i].desc));
+		sensor_attach(&lpc->sc_sensordev, &lpc->sc_sensors[i]);
+	}
+	sensordev_install(&lpc->sc_sensordev);
+	reformlpc_sensors_update(lpc);
+
+#if NAPM > 0
+	// set apm data and set up apm data query hook
+	apm_setinfohook(reformlpc_apminfo);
+#endif
+
+	// set kernel powerdown function
+	powerdownfn = reformlpc_powerdown;
+
+	printf("\n");
+}
+
+void
+reformlpc_powerdown(void)
+{
+	struct reformlpc_softc *lpc = reformlpc_sc;
+	char response_buffer[REFORMLPC_PACKET_SIZE];
+
+        reformlpc_command(lpc, REFORMLPC_CMD_POWER, REFORMLPC_POWER_OFF, response_buffer);
+}
+
+void
+reformlpc_sensors_update(void *vsc)
+{
+	struct reformlpc_softc *lpc = (struct reformlpc_softc *)vsc;
+	struct reformlpc_query_data response;
+	int16_t bat_amps;
+	int16_t bat_volts;
+        bool bat_charging;
+	bool ac_connected;
+	struct ksensor *ks;
+#if NAPM > 0
+	struct apm_power_info prev_apm_data;
+#endif
+
+	// invalidate sensor readings
+	for (int i = 0; i < nitems(lpc->sc_sensors); i++)
+		lpc->sc_sensors[i].flags |= SENSOR_FINVALID;
+
+	// send query command
+        reformlpc_command(lpc, REFORMLPC_CMD_STATUS_QUERY, 0x0, (char *)&response);
+	// convert response values
+        bat_volts = ((int32_t)response.voltage[0] | ((int32_t)response.voltage[1] << 8));
+	bat_amps = ((int32_t)response.amps[0] | ((int32_t)response.amps[1] << 8));
+	bat_charging = (response.status == REFORM_ST_CHARGE) && bat_amps < 0;
+	ac_connected = bat_amps <= 0;
+
+#if NAPM > 0
+	// set up apm data
+ 	memcpy(&prev_apm_data, &reformlpc_apmdata, sizeof(struct apm_power_info));
+
+	// set apm battery percent
+	reformlpc_apmdata.battery_life = response.bat_percent;
+#endif
+	// set battery percent sensor
+        ks = &lpc->sc_sensors[REFORMLPC_SENSOR_BATTERY_PERCENT];
+	ks->value = response.bat_percent * 1000;
+	ks->flags &= ~SENSOR_FINVALID;
+
+	// set AC status based on charging or charged state
+	ks = &lpc->sc_sensors[REFORMLPC_SENSOR_AC_PRESENCE];
+	ks->flags &= ~SENSOR_FINVALID;
+	if (ac_connected) {
+	    ks->value = 1;
+#if NAPM > 0
+	    reformlpc_apmdata.ac_state = APM_AC_ON;
+#endif
+	} else {
+	    ks->value = 0;
+#if NAPM > 0
+	    reformlpc_apmdata.ac_state = APM_AC_OFF;
+#endif
+        }
+
+	// set charge status
+	ks = &lpc->sc_sensors[REFORMLPC_SENSOR_BATTERY_CHARGING];
+	ks->value = bat_charging;
+	ks->flags &= ~SENSOR_FINVALID;
+
+	// set voltage sensor
+ 	ks = &lpc->sc_sensors[REFORMLPC_SENSOR_BATTERY_VOLTAGE];
+	ks->value = bat_volts * 1000;
+	ks->flags &= ~SENSOR_FINVALID;
+
+	// set amp sensor
+ 	ks = &lpc->sc_sensors[REFORMLPC_SENSOR_BATTERY_CURRENT];
+	ks->value = bat_amps * 1000;
+	ks->flags &= ~SENSOR_FINVALID;
+
+#if NAPM > 0
+	// set APM battery status
+	if (bat_charging)
+	         reformlpc_apmdata.battery_state = APM_BATT_CHARGING;
+	else if (response.bat_percent > 50)
+	         reformlpc_apmdata.battery_state = APM_BATT_HIGH;
+	else if (response.bat_percent > 25)
+	         reformlpc_apmdata.battery_state = APM_BATT_LOW;
+        else
+	         reformlpc_apmdata.battery_state = APM_BATT_CRITICAL;
+	reformlpc_apmdata.minutes_left = -1;
+	if ((prev_apm_data.ac_state != reformlpc_apmdata.ac_state) ||
+	    (prev_apm_data.battery_state != reformlpc_apmdata.battery_state))
+	        apm_record_event(APM_POWER_CHANGE);
+	if ((prev_apm_data.battery_state != reformlpc_apmdata.battery_state) &&
+	    (reformlpc_apmdata.battery_state == APM_BATT_LOW))
+	        apm_record_event(APM_BATTERY_LOW);
+#endif
+}
+
+#if NAPM > 0
+int
+reformlpc_apminfo(struct apm_power_info *info)
+{
+         memcpy(info, &reformlpc_apmdata, sizeof(struct apm_power_info));
+	 return 0;
+}
+#endif
+
+void
+reformlpc_command(struct reformlpc_softc *lpc, char cmd, char arg, char *response_buf)
+{
+        // build command, including magic
+        char command_buf[REFORMLPC_PACKET_SIZE] = {REFORMLPC_MAGIC, cmd, arg, 0x0, 0x0, 0x0, 0x0, 0x0};
+
+	spi_acquire_bus(lpc->sc_spi_tag, 0);
+	spi_config(lpc->sc_spi_tag, &lpc->sc_spi_conf);
+
+	// write command to lpc
+	if (spi_transfer(lpc->sc_spi_tag, command_buf, response_buf, REFORMLPC_PACKET_SIZE, 0)) {
+		printf("%s: cannot write to lpc\n", lpc->sc_dev.dv_xname);
+	}
+
+	spi_release_bus(lpc->sc_spi_tag, 0);
+}
