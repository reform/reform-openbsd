#!/bin/sh
set -x
set -e

STABLE_MIRROR="https://cdn.openbsd.org/pub/OpenBSD/7.4/arm64/install74.img"
SNAPSHOT_MIRROR="https://cdn.openbsd.org/pub/OpenBSD/snapshots/arm64/install75.img"

echo "fetching images"
# fetch stable OpenBSD installer
curl -C - "${STABLE_MIRROR}" -O
# fetch snapshot OpenBSD installer
curl -C - "${SNAPSHOT_MIRROR}" -O
# build uboot with reform config
echo "cleaning uboot"
make distclean
echo "building uboot"
make imx8mq_reform2_defconfig
ARCH=arm CROSS_COMPILE=aarch64-unknown-linux-gnu- make flash.bin
# write new uboot at correct offset
echo "writing uboot to images"
cp install74.img install74-reform2.img
cp install75.img install75-reform2.img
dd if=flash.bin of=install74-reform2.img conv=notrunc bs=1k seek=33
dd if=flash.bin of=install75-reform2.img conv=notrunc bs=1k seek=33
