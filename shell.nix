{ pkgs ? import <nixpkgs> {} }:
let
  nixpkgs = import <nixpkgs> {};
  armpkgs = import <nixpkgs> { crossSystem = { config = "aarch64-unknown-linux-gnu"; }; };
in nixpkgs.mkShell {
  # host platform
  buildInputs = [
    armpkgs.buildPackages.openssl
    armpkgs.buildPackages.gcc
    armpkgs.binutils
  ];
  # build system
  nativeBuildInputs = [
    nixpkgs.python3
    nixpkgs.python3.pkgs.setuptools
    nixpkgs.ncurses
    nixpkgs.qt5.full
    nixpkgs.pkg-config
    nixpkgs.bc
    nixpkgs.dtc
    nixpkgs.swig
  ];
}


